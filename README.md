# Description
This is a python 🐍 a Hello World! 👋 🌎 project with flask 🌶️ micro framework for learning and understand flask

# Make with
[![Python](https://img.shields.io/badge/python-2b5b84?style=for-the-badge&logo=python&logoColor=white&labelColor=000000)]()
[![Flask](https://img.shields.io/badge/flask-000000?style=for-the-badge&logo=flask&logoColor=white&labelColor=000000)]()